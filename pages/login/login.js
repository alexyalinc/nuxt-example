export default {
  middleware: "auth",
  data() {
    return {
      user: {
        login: "",
        password: ""
      }
    };
  },
  methods: {
    login(e) {
      e.preventDefault();
      if (this.user.login.length > 0 && this.user.password.length > 0) {
        this.$axios.$post("/login", this.user).then(res => {
          if (res.status) {
            this.$store.commit("user/setAuth", true);
            this.$router.push({ path: "/posts/edit" });
          }
        });
      }
    }
  }
};

import Post from "./components/Post.vue";
import { cloneDeep } from "lodash"

export default {
  components: {
    Post
  },

  computed: {
    sortedPosts() {
      const posts = cloneDeep(this.$store.state.posts.data);
      return posts.sort((a, b) => {
        return new Date(b.date) - new Date(a.date);
      });
    }
  },
  
  async fetch({ store, $axios }) {
    const posts = await $axios.$get("/posts");
    await store.commit("posts/setPosts", posts);
  }
};

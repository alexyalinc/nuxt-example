import Post from "./components/Post.vue";
import PostForm from "./components/PostForm.vue";
import { cloneDeep } from "lodash";

export default {
  middleware: "notAuth",

  components: {
    Post
  },

  computed: {
    sortedPosts() {
      const posts = cloneDeep(this.$store.state.posts.data);
      return posts.sort((a, b) => {
        return new Date(b.date) - new Date(a.date);
      });
    }
  },

  async fetch({ store, $axios }) {
    const posts = await $axios.$get("/posts");
    await store.commit("posts/setPosts", posts);
  },

  methods: {
    createPost(post) {
      this.$store.dispatch("modal/open", {
        title: `<strong>Создать статью</strong>`,
        component: PostForm
      });
    }
  }
};

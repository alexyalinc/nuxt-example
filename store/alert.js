export const state = () => ({
  open: false,
  text: '',
  noCB: null,
  yesCB: null,
});

export const mutations = {
  setAlert(state, data) {
    state = data;
  },
  setOpen(state, open) {
    state.open = open;
  },
  setNoCB(state, noCB) {
    state.noCB = noCB;
  },
  setYesCB(state, yesCB) {
    state.yesCB = yesCB;
  },
  setText(state, text) {
    state.text = text;
  }
};

export const actions = {
  open({ commit }, data) { // data object. yes - callback function, no - callback function, text - string
    if (process.client) {
      document.body.style.overflow = 'hidden';
    }
    commit("setText", data.text);
    commit("setYesCB", data.yes);
    commit("setNoCB", data.no);
    commit("setOpen", true);
  },

  close({ commit }) {
    if (process.client) {
      document.body.style.overflow = 'auto';
    }
    commit("setAlert", {
      open: false,
      text: '',
      noCB: null,
      yesCB: null,
    });
    commit("setOpen", false);
  }
};

export const state = () => ({
  auth: false
});

export const mutations = {
  setAuth(state, auth) {
    state.auth = auth;
  }
};

export const actions = {
  logout({ commit }) {
    this.$axios.$get("/logout").then(res => {
      if (res.status) {
        commit("setAuth", false);
        this.$router.push({ path: "/" });
      }
    });
  }
};

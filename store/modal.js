export const state = () => ({
  open: false,
  title: '',
  component: null,
  data: null
});

export const mutations = {
  setModal(state, data) {
    state = data;
  },
  setOpen(state, open) {
    state.open = open;
  },
  setTitle(state, title) {
    state.title = title;
  },
  setData(state, data) {
    state.data = data;
  },
  setComponent(state, component) {
    state.component = component;
  }
};

export const actions = {
  open({ commit }, data) { // data object. component - vue component, title - string, data - any
    if (process.client) {
      document.body.style.overflow = 'hidden';
    }
    commit("setTitle", data.title);
    commit("setComponent", data.component);
    commit("setData", data.data);
    commit("setOpen", true);
  },

  close({ commit }) {
    if (process.client) {
      document.body.style.overflow = 'auto';
    }
    commit("setModal", {
      open: false,
      title: '',
      component: null,
      data: null
    });
    commit("setOpen", false);
  }
};

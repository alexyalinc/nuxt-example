export const actions = {
  nuxtServerInit({ commit }, { req }) {
    commit("user/setAuth", req.session.auth ? true : false);
  }
};

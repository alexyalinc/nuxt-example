export const state = () => ({
  data: []
});

export const mutations = {
  setPosts(state, posts) {
    state.data = posts;
  }
};

export const actions = {
  set({ commit }, posts) {
    this.$axios.put("/posts", posts).then(res => {
      if (res.status) {
        commit("setPosts", posts);
      }
    });
  }
};

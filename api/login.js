export default router => {
  router.post("/login", (req, res) => {
    req.session.auth = true;
    return res.json({ status: true });
  });
  router.get("/logout", (req, res) => {
    req.session.auth = false;
    return res.json({ status: true })
  });
};

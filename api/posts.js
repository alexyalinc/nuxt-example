export default router => {
  router.get("/posts", (req, res) => {
    if (!req.session.posts) {
      req.session.posts = [
        {
          id:1,
          text:
            "Laboris culpa ex quis ea elit ex eu. Ut dolore exercitation laboris tempor voluptate. Veniam fugiat aute fugiat non. Velit ad aliqua incididunt aliqua velit reprehenderit tempor enim nisi incididunt do. Qui proident cupidatat dolor ad nulla Lorem esse ad magna tempor labore. Aliquip qui anim fugiat dolor consectetur anim.",
          title:
            "Cillum anim aliqua officia aute voluptate culpa anim amet deserunt in.",
          date: "2019-09-21T16:15:31.562Z"
        },
        {
          id:2,
          text:
            "Excepteur nostrud qui sunt consequat sit velit cupidatat aute voluptate excepteur dolor exercitation aute nostrud. Magna id Lorem laboris id deserunt labore esse. Magna ut occaecat occaecat et sunt nostrud.",
          title: "Do tempor et magna est Lorem consectetur deserunt anim ad.",
          date: "2019-09-26T16:15:31.562Z"
        },
        {
          id:3,
          text:
            "Esse esse irure occaecat consequat enim consectetur proident. Sit anim fugiat id occaecat in velit culpa voluptate officia qui. Ea consequat nisi et sit non reprehenderit aliquip fugiat aliquip proident. Velit ipsum eu magna anim sint officia elit deserunt id esse adipisicing anim.",
          title: "Lorem et ad ex nisi commodo fugiat ullamco commodo laboris.",
          date: "2019-09-25T16:15:31.562Z"
        },
        {
          id:4,
          text:
            "In qui commodo cupidatat mollit Lorem labore eu enim. Irure veniam mollit nostrud aliquip adipisicing ad ea occaecat ut exercitation aute. Ex nisi id minim excepteur aute nostrud qui. Id labore minim veniam est aliqua eiusmod.",
          title:
            "Irure laborum aliquip reprehenderit irure enim quis ea sit dolore.",
          date: "2019-09-26T16:15:31.562Z"
        },
        {
          id:5,
          text:
            "Ad ut anim cupidatat sint est mollit sit incididunt aute nisi non quis. Aliquip ipsum sit est do exercitation nulla aute nulla mollit consectetur exercitation est. Ad nostrud eiusmod proident adipisicing qui sunt ullamco esse ut laboris. Id do commodo nisi est laborum est dolor.",
          title:
            "Eu tempor commodo eu enim ullamco minim aliquip veniam ea nulla pariatur officia.",
          date: "2019-09-23T16:15:31.562Z"
        },
        {
          id:6,
          text:
            "Nulla ex occaecat ea voluptate id in id. In culpa ex sit aliquip labore enim labore. Culpa sunt ullamco culpa officia sit laborum proident eiusmod sint dolore. Exercitation labore commodo voluptate magna officia Lorem duis est aliquip labore dolor proident. In ex veniam excepteur sint. Cillum qui consequat officia ad ad. Tempor consectetur eiusmod pariatur deserunt.",
          title: "Deserunt exercitation esse deserunt anim.",
          date: "2019-09-24T16:15:31.562Z"
        }
      ];
    }
    return res.json(req.session.posts);
  });

  
  router.put("/posts", (req, res) => {
    req.session.posts = req.body;
    return res.json({ status: true });
  });

};

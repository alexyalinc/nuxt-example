import express from 'express'
// ROUTES 
import postsRoutes from './posts.js';
import loginRoutes from './login.js';

const router = express.Router()

const app = express()
router.use((req, res, next) => {
  Object.setPrototypeOf(req, app.request)
  Object.setPrototypeOf(res, app.response)
  req.res = res
  res.req = req
  next()
})

//ADD ROUTES
postsRoutes(router);
loginRoutes(router);

export default router
export default [
    {
        name: 'error404',
        path: '*',
        component: '~/pages/errors/404.vue',
        meta: {
            type: 'hidden',
            permits: []
        }
    },
    
    {
        name: 'Posts',
        path: '/',
        component: '~/pages/posts/posts.vue',
        meta: {
            type: 'hidden',
            title: 'Posts',
            permits: ['guest', 'auth']
        }
    },
    
    {
        name: 'Posts edit',
        path: '/posts/edit',
        component: '~/pages/postsEdit/postsEdit.vue',
        meta: {
            type: 'web',
            title: 'Posts edit',
            permits: ['auth']
        }
    },
    
    {
        name: 'Login',
        path: '/login',
        component: '~/pages/login/login.vue',
        meta: {
            type: 'web',
            title: 'Login',
            permits: ['guest']
        }
    }
]
import Vue from 'vue';

const req = require.context('../components/', false, /\.(js|vue)$/i)
for (const key of req.keys()) {
  const name = key.match(/\w+/)[0]
  Vue.component(name, req(key).default)
}
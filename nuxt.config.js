
import bodyParser from 'body-parser'
import session from 'express-session'
import cusotmRoutes from './routes/index.js'

const HOST_SERVER = '0.0.0.0';
const PORT = 3000;
const HOST_API = 'localhost';

export default {
  mode: 'universal',
  server: {
    port: PORT,
    host: HOST_SERVER,
  },
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
    { src: '~/assets/css/index.css', lang: 'pcss' }
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/registerGlobalComponents.js'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: `http://${HOST_API}:${PORT}/api`,
    browserBaseURL: `http://${HOST_API}:${PORT}/api`,
  },
  /*
  ** Layout Transition
  */
  layoutTransition: {
    name: 'layout',
    mode: 'out-in'
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend (config, ctx) {
    },
    postcss: {
      plugins: {
        'postcss-nested': {},
      },
      preset: {
        autoprefixer: {
          grid: true
        }
      }
    }
  },
  /*
  ** Server middleware
  */
  serverMiddleware: [
    bodyParser.json(),
    // session middleware
    session({
      secret: 'super-secret-key',
      resave: false,
      saveUninitialized: false,
      cookie: { maxAge: 60000*100 }
    }),

    { path: '/api', handler: '~/api/index.js' },
  ],
  router: {
    extendRoutes (routes, resolve) {
      routes.length = 0;
      for(let route of cusotmRoutes) {
        routes.push(route);
      }
    }
  }
}
